#!/bin/ash
set -e

# Set the time zone
if [ ! -z "$APP_TIMEZONE" ]; then
  cp "/usr/share/zoneinfo/$APP_TIMEZONE" /etc/localtime
  echo "$APP_TIMEZONE" > /etc/timezone
  sed -i "s|^date.timezone =.*|date.timezone = $APP_TIMEZONE|" /etc/php7/php.ini
fi

FILE=/app/config/config_inc.php
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    rm -rf admin
    tar -zxvf /mantisbt-admin.tgz > /dev/null 2>&1
    chown -R nginx:nginx admin
fi

exec "$@"
