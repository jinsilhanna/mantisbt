# mantisbt

```bash
docker exec mantisbt cat /app/config/config_inc.php > config_inc.php
```

## 참조

* [https://gitlab.com/daverona/docker/mantisbt](https://gitlab.com/daverona/docker/mantisbt)

## 감사글

* thanks to egkim